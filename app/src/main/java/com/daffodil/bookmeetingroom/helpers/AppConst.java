package com.daffodil.bookmeetingroom.helpers;

/**
 * com.daffodil.bookmeetingroom.helpers . AppConst
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class AppConst {
    public static String CLIENT_ID = "679952827554-g6r636ocmrldigedpffma3qeu4n8unrf.apps.googleusercontent.com";
    public static String TAG = "bookmeetingroom";
    public static String PREFERENCE_KEY = "bookmeetingroom";
    public static String GOOGLE_LOGIN_ID_KEY = "loginid";
    public static String LOGIN_MODEL_KEY = "loginmodel";
    public static String USER_ID_KEY = "userId";
    public static String TOKEN_ID = "token";
    public static String SELECTED = "selected";
    public static String IS_MY_BOOK = "ismybooking";

    public static String[] monthArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static int VIEW_TYPE_TAB_LAYOUT = 0;
    public static int VIEW_TYPE_MONTH = 1;
    public static int VIEW_TYPE_CALENDER = 2;
    public static int VIEW_TYPE_DATA = 3;
    public static int VIEW_TYPE_DETAILS = 4;

    public static String API_KEY = "c112f68c22a4b6ff1240e072f64677d155b3d05488bcf5c5194b88aef04ea765";
    public static String BASE_URL = "http://172.18.1.9:8722";

    public static String HEADER_KEY_1 = "Content-Type";
    public static String HEADER_KEY_2 = "api_key";
    public static String HEADER_KEY_3 = "auth_token";
    public static String HEADER_VALUE_1 = "application/json";

    public static String BODY_KEY_1 = "email";
    public static String BODY_KEY_2 = "googlePlusId";
    public static String BODY_KEY_3 = "firstName";

    public static String BODY_KEY_4 = "from_date";
    public static String BODY_KEY_5 = "to_date";
    public static String BODY_KEY_6 = "resourceId";
    public static String BODY_KEY_7 = "skip";
    public static String BODY_KEY_8 = "limit";

    public static String BODY_KEY_9 = "date";
    public static String BODY_KEY_10 = "schedule";
    public static String BODY_KEY_11 = "from";
    public static String BODY_KEY_12 = "to";
    public static String BODY_KEY_13 = "title";
    public static String BODY_KEY_14 = "description";

    public static String BODY_KEY_15 = "from_date";
    public static String BODY_KEY_16 = "to_date";

}
