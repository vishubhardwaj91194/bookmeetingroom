package com.daffodil.bookmeetingroom.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * com.daffodil.bookmeetingroom.helpers . DateHelper
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class DateHelper {
    public static long dateTimeToTimestamp(String dateTime) {

        long time = 0;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd-kk.mm");
        df.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        try {
            Date parsedDate = df.parse(dateTime);
            time = parsedDate.getTime();
        }catch(ParseException e){}
        return time;
    }

    public static Calendar convertTimeWithTimeZome(long time){
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT +0500"));
        cal.setTimeInMillis(time);
//        return (cal.get(Calendar.YEAR) + " " + (cal.get(Calendar.MONTH) + 1) + " "
//                + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
//                + cal.get(Calendar.MINUTE));
        return cal;

    }

    public static long getCurrentTimestamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT +0530"));
        return calendar.getTimeInMillis();
    }
}
