package com.daffodil.bookmeetingroom.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daffodil.bookmeetingroom.activities.homeactivity.presenter.HomePresenter;
import com.daffodil.bookmeetingroom.activities.homeactivity.presenter.iHomePresenter;
import com.daffodil.bookmeetingroom.activities.mainactivity.presenter.iMainPresenter;

import org.json.JSONObject;

import java.util.Map;

/**
 * com.daffodil.bookmeetingroom.helpers . ApiManager
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ApiManager {
    public static void sendUserLoginApi(String url, final Map<String, String> header, final JSONObject body, final iMainPresenter listener, Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onUserLoginSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, new String(error.networkResponse.data));
                    }
                }
        ){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return body.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }

    public static void sendCategoriesApi(String url, final Map<String, String> header, final iHomePresenter listener, Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onLoadCategoriesSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, error.toString());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }

    public static void sendBookingListForResource(String url, final Map<String, String> header, final JSONObject body, final iHomePresenter listener, Context context){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onBookingListForResource(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, new String(error.networkResponse.data));
                    }
                }
        ){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return body.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }

    public static void sendDEleteBookingApi(String url, final Map<String, String> header, final iHomePresenter listener, Context context, final int startPos, final int numItems) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.DELETE,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onDeleteSuccess(response, startPos, numItems);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, new String(error.networkResponse.data));
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }

    public static void sendBookSingleDevice(String url, final Map<String, String> header, final JSONObject body, Context context, final iHomePresenter listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onSingleBookingSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, new String(error.networkResponse.data));
                        listener.onSingleBookingError(new String(error.networkResponse.data));
                    }
                }
        ){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return body.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }

    public static void sendBookMultipleDevice(String url, final Map<String, String> header, final JSONObject body, Context context, final HomePresenter listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(AppConst.TAG, response);
                        listener.onMultipleBookingSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(AppConst.TAG, new String(error.networkResponse.data));
                        listener.onMultipleBookingError(new String(error.networkResponse.data));
                    }
                }
        ){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return body.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }
        };
        queue.add(request);
    }
}
