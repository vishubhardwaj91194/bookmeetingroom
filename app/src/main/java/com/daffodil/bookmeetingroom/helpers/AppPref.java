package com.daffodil.bookmeetingroom.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * com.daffodil.bookmeetingroom.helpers . AppPref
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class AppPref {

    public static String getGoogleLoginID(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        return sharedPref.getString(
                AppConst.GOOGLE_LOGIN_ID_KEY,
                null
        );
    }

    public static void setGoogleLoginId(String id, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        Editor editor = sharedPref.edit();
        editor.putString(
                AppConst.GOOGLE_LOGIN_ID_KEY,
                id
        );
        editor.commit();
    }

    public static void setUserId(String userId, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        Editor editor = sharedPref.edit();
        editor.putString(
                AppConst.USER_ID_KEY,
                userId
        );
        editor.commit();
    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        return sharedPref.getString(
                AppConst.USER_ID_KEY,
                null
        );
    }

    public static void setToken(String token, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        Editor editor = sharedPref.edit();
        editor.putString(
                AppConst.TOKEN_ID,
                token
        );
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        return sharedPref.getString(
                AppConst.TOKEN_ID,
                null
        );
    }

    public static int getSelected(Context context, int def) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        return sharedPref.getInt(
                AppConst.SELECTED,
                def
        );
    }

    public static void setSelected(Context context, int sel) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        Editor editor = sharedPref.edit();
        editor.putInt(
                AppConst.SELECTED,
                sel
        );
        editor.commit();
    }

    public static boolean getIsMyBooking(Context context, boolean def) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        return sharedPref.getBoolean(
                AppConst.IS_MY_BOOK,
                def
        );
    }

    public static void setIsMyBooking(Context context, boolean isMyBook) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                AppConst.PREFERENCE_KEY,
                Context.MODE_PRIVATE
        );
        Editor editor = sharedPref.edit();
        editor.putBoolean(
                AppConst.IS_MY_BOOK,
                isMyBook
        );
        editor.commit();
    }
}
