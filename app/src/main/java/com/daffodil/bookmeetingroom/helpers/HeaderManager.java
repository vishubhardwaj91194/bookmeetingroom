package com.daffodil.bookmeetingroom.helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * com.daffodil.bookmeetingroom.helpers . HeaderManager
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class HeaderManager {
    public static Map<String, String> getSimpleMap() {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(AppConst.HEADER_KEY_1, AppConst.HEADER_VALUE_1);
        headerMap.put(AppConst.HEADER_KEY_2, AppConst.API_KEY);
        return headerMap;
    }

    public static Map<String, String> getComplexMap(String token) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(AppConst.HEADER_KEY_1, AppConst.HEADER_VALUE_1);
        headerMap.put(AppConst.HEADER_KEY_3, token);
        headerMap.put(AppConst.HEADER_KEY_2, AppConst.API_KEY);
        return headerMap;
    }
}
