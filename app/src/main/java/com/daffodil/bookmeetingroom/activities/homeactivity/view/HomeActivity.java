package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.Resource;
import com.daffodil.bookmeetingroom.activities.mainactivity.model.GmailDataModel;
import com.daffodil.bookmeetingroom.activities.mainactivity.view.MainActivity;
import com.daffodil.bookmeetingroom.helpers.AppConst;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.List;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, iHomeActivity {

    private GmailDataModel model;
    private GoogleApiClient mGoogleApiClient;
    private String name, email, photo;
    private ShowBookingsFragment showBooking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Show Booking");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        ImageView imageView = (ImageView) navHeader.findViewById(R.id.imageView);
        TextView name = (TextView) navHeader.findViewById(R.id.name);
        TextView email = (TextView) navHeader.findViewById(R.id.email);

        model = getIntent().getExtras().getParcelable(AppConst.LOGIN_MODEL_KEY);
        if(model != null) {
            this.name = model.name;
            this.email = model.email;
            if (model.photoUri != null) {
                this.photo = model.photoUri.toString();
                //imageView.setImageURI(model.photoUri);
                Glide
                        .with(getApplicationContext())
                        .load(photo)
                        .thumbnail(0.5f)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
            name.setText(this.name);
            email.setText(this.email);
        } else if (savedInstanceState != null) {
            this.name = savedInstanceState.getString("name");
            this.email = savedInstanceState.getString("email");
            this.photo = savedInstanceState.getString("photo");
            name.setText(this.name);
            email.setText(this.email);
            Glide
                    .with(getApplicationContext())
                    .load(this.photo)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

if(savedInstanceState == null) {
    AppPref.setIsMyBooking(this, false);
    showBooking = ShowBookingsFragment.getInstance();
    showBooking.setActivity(this);
    showBooking.setContext(this);
    getFragmentManager()
            .beginTransaction()
            .replace(
                    R.id.container,
                    showBooking
            )
            .commit();
}

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().findFragmentByTag("FragmentB") != null) {
            // I'm viewing Fragment C
            getSupportFragmentManager().popBackStack(
                    "A_B_TAG",
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
            );
        } else if (getSupportFragmentManager().findFragmentByTag("FragmentC") != null) {
            // I'm viewing Fragment C
            getSupportFragmentManager().popBackStack(
                    "A_C_TAG",
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
            );
        } else {
            getSupportActionBar().setTitle("Show Booking");
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.calender_top) {
            showBooking.onCalClick();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.logout:
                signOut();
                break;
            case R.id.show_bookings:
                AppPref.setIsMyBooking(this, false);
                showBooking.resetBookings();
                break;
            case R.id.my_bookings:
                AppPref.setIsMyBooking(this, true);
                showBooking.resetBookings();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        //updateUI(false);
                        AppPref.setGoogleLoginId(null, HomeActivity.this);
                        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                        startActivity(intent);
                        HomeActivity.this.finish();
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayEditBook(ViewDetailsModel model) {
        EditBookingFragment showBooking = new EditBookingFragment();
        showBooking.setActivity(this);
        showBooking.setData(model);
        getSupportActionBar().setTitle("EditBooking");
        showBooking.setContext(this);
        getFragmentManager()
                .beginTransaction()
                .replace(
                        R.id.container,
                        showBooking,
                        "FragmentB"
                )
                .addToBackStack("A_B_TAG")
                .commit();
    }

    @Override
    public void onEditBookingCancel() {
        this.onBackPressed();
    }

    @Override
    public void displayBookDevice(int dd, int mm, int yy, int from_hh, int from_mm,  List<Resource> resources, int position) {
        BookDeviceFragment newBook = new BookDeviceFragment();
        newBook.setActivity(this);
        getSupportActionBar().setTitle("Book Meeting Room");
        newBook.setData(dd, mm, yy, from_hh, from_mm, resources, position);
        newBook.setContext(this);
        getFragmentManager()
                .beginTransaction()
                .replace(
                        R.id.container,
                        newBook,
                        "FragmentC"
                )
                .addToBackStack("A_C_TAG")
                .commit();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("name", name);
        outState.putString("email", email);
        outState.putString("photo", photo);

    }
}
