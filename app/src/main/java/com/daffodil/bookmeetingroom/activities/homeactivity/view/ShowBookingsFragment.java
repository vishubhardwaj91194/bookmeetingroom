package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.adapter.ShowBookingAdapter;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.CalenderModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.MonthModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ShowBookingBaseModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.SimpleModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.TabLayoutModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.CategoryResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.Resource;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.Item;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.Schedule;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.ShowBookByResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.presenter.HomePresenter;
import com.daffodil.bookmeetingroom.helpers.AppConst;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.daffodil.bookmeetingroom.helpers.DateHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import butterknife.ButterKnife;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ShowBookingsFragment
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ShowBookingsFragment extends Fragment implements iShowBooking {

    private Context context;
    private View view;
    private RecyclerView mRecyclerView;
    private ArrayList<ShowBookingBaseModel> data = null;
    private LinearLayoutManager mLayoutManager;
    private ShowBookingAdapter adapter;
    private iHomeActivity activity;
    private HomePresenter presenter;
    private boolean isFullLoaded = false;
    private static ShowBookingsFragment fragment;

    public static ShowBookingsFragment getInstance() {
        if (fragment == null) {
            fragment = new ShowBookingsFragment();
        }
        return fragment;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setActivity(iHomeActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_booking, container, false);
        this.view = view;
        ButterKnife.bind(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            try {
                this.data = (ArrayList<ShowBookingBaseModel>) savedInstanceState.getSerializable("TheMainModel");
                isFullLoaded = true;
                // onSaveInstanceState(savedInstanceState);
            } catch (NullPointerException e) {
                data = new ArrayList<>();
                e.printStackTrace();
            }

        } else {
            data = new ArrayList<>();
        }


        if (context == null) {
            context = view.getContext();
        }
        presenter = new HomePresenter(context);
// data = new ArrayList<>();
        startRecyclerView();
        if (this.data.size() == 0) {
            presenter.tryLoadCategories(this);
        }

        // initRecyclerView();
    }

    @Override
    public void onYearChange(int year) {
        int month = ((CalenderModel) data.get(2)).month;
        MonthModel monthModel = (MonthModel) data.get(1);
        if (monthModel.year != year) {
            data.remove(1);
            monthModel.year = year;
            monthModel.monthYearText = AppConst.monthArray[month] + ", " + year;
            data.add(1, monthModel);
            adapter.updateData(data);
            // adapter.notifyItemChanged(1);


            CalenderModel calenderModel = (CalenderModel) data.get(2);
            data.remove(2);
            calenderModel.year = year;
            calenderModel.date = 1;
            calenderModel.currDate = 1;

            data.add(2, calenderModel);
            adapter.updateData(data);
            // adapter.notifyItemChanged(2);

            TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
            for (int i = 0; i < 49; i++) {
                data.remove(3);
                adapter.updateData(data);
                adapter.notifyDataSetChanged();
            }
            String from_date = String.format("%4d", monthModel.year) + "-" + String.format("%02d", calenderModel.month + 1) + "-" + String.format("%02d", 1);
            presenter.sendViewBookingOnResource(from_date, from_date, tabLayoutModel.selectedId, "0", "100", this);
        }
    }

    @Override
    public void onChangeMonth(int year, int month, int date) {
        boolean flag = false;
        MonthModel monthModel = (MonthModel) data.get(1);
        if (monthModel.year == year && monthModel.month == month) {

        } else {
            flag = true;
            data.remove(1);
            monthModel.month = month;
            monthModel.year = year;
            monthModel.monthYearText = AppConst.monthArray[month] + ", " + year;
            if (month == 0) {
                monthModel.prevMonthText = AppConst.monthArray[11];
                monthModel.nextMonthText = AppConst.monthArray[1];
            } else if (month == 11) {
                monthModel.prevMonthText = AppConst.monthArray[10];
                monthModel.nextMonthText = AppConst.monthArray[0];
            } else {
                monthModel.prevMonthText = AppConst.monthArray[((month - 1) == -1) ? 11 : (month - 1)];
                monthModel.nextMonthText = AppConst.monthArray[(month + 1) % 12];
            }
            data.add(1, monthModel);
            adapter.updateData(data);
            // adapter.notifyItemChanged(1);
        }

        CalenderModel calenderModel = (CalenderModel) data.get(2);
        if (calenderModel.year == year && calenderModel.month == month && calenderModel.date == date && calenderModel.currDate == date) {

        } else {
            flag = true;
            data.remove(2);
            calenderModel.year = year;
            calenderModel.month = month;
            calenderModel.date = date;
            calenderModel.currDate = date;
            data.add(2, calenderModel);
            adapter.updateData(data);
            // adapter.notifyItemChanged(2);
        }

        if (flag == true) {
            TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
            for (int i = 0; i < 49; i++) {
                data.remove(3);
                adapter.updateData(data);
                adapter.notifyDataSetChanged();
            }
            String from_date = String.format("%4d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", date);
            presenter.sendViewBookingOnResource(from_date, from_date, tabLayoutModel.selectedId, "0", "100", this);
        }
    }

    @Override
    public void onTabChanged(int position) {
        AppPref.setSelected(context, position);
        TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
        if (tabLayoutModel.selected != position) {
            CalenderModel calenderModel = (CalenderModel) data.get(2);
            int d = calenderModel.currDate;
            int m = calenderModel.month;
            int y = calenderModel.year;

            data.remove(0);
            tabLayoutModel.selected = position;
            tabLayoutModel.selectedId = tabLayoutModel.resources.get(position).getId();
            data.add(0, tabLayoutModel);
            for (int i = 0; i < 49; i++) {
                data.remove(3);
            }
            // data.removeAll(data.subList(4,52));
            adapter.updateData(data);
            // adapter.notifyItemChanged(0);
            adapter.notifyDataSetChanged();

            String from_date = String.format("%4d", y) + "-" + String.format("%02d", m + 1) + "-" + String.format("%02d", d);
            presenter.sendViewBookingOnResource(from_date, from_date, tabLayoutModel.selectedId, "0", "100", this);
        }
    }

    public void resetBookings() {
        TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
        CalenderModel calenderModel = (CalenderModel) data.get(2);
        int d = calenderModel.currDate;
        int m = calenderModel.month;
        int y = calenderModel.year;
        for (int i = 0; i < 49; i++) {
            data.remove(3);
        }
        adapter.updateData(data);
        adapter.notifyDataSetChanged();
        String from_date = String.format("%4d", y) + "-" + String.format("%02d", m + 1) + "-" + String.format("%02d", d);
        presenter.sendViewBookingOnResource(from_date, from_date, tabLayoutModel.selectedId, "0", "100", this);
    }

    @Override
    public void showYearSpinner() {
        FragmentManager fm = ((Activity) context).getFragmentManager();
        SelectYearDF yearDF = SelectYearDF.getInstance();
        yearDF.setContext(context);
        yearDF.setCurrentYear(((CalenderModel) data.get(2)).year);
        // yearDF.setCurrentYear(mcv.getCurrentDate().getYear());
        yearDF.setListener(this);
        yearDF.show(fm, "yearDF");
    }

    @Override
    public void changeDate(int day) {
        CalenderModel calenderModel = (CalenderModel) data.get(2);
        if (calenderModel.date != day || calenderModel.currDate != day) {
            data.remove(2);
            calenderModel.date = day;
            calenderModel.currDate = day;
            data.add(2, calenderModel);
            adapter.updateData(data);
            // adapter.notifyItemChanged(2);

            TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
            MonthModel monthModel = (MonthModel) data.get(1);
            for (int i = 0; i < 49; i++) {
                data.remove(3);
                adapter.updateData(data);
                adapter.notifyDataSetChanged();
            }
            String from_date = String.format("%4d", monthModel.year) + "-" + String.format("%02d", monthModel.month + 1) + "-" + String.format("%02d", day);
            presenter.sendViewBookingOnResource(from_date, from_date, tabLayoutModel.selectedId, "0", "100", this);

        }
    }

    @Override
    public void onEditClick(ViewDetailsModel model) {
        // activity.displayEditBook(model);
    }

    @Override
    public void showBooking(ViewDetailsModel model, int position, ViewDetailsModel viewDetailsModel) {
        FragmentManager fm = ((Activity) context).getFragmentManager();
        ShowBookingDF bookingDF = ShowBookingDF.getInstance();
        bookingDF.setContext(context);
        bookingDF.setModel(model);
        bookingDF.show(fm, "bookingDF");
    }

    @Override
    public void showBookDevice(int dd, int mm, int yy, int from_hh, int from_mm) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT +0530"));
        long y = calendar.getTimeInMillis();
        String date = String.format("%4d", yy) + "-" + String.format("%02d", mm) + "-" + String.format("%02d", dd) + "-" + String.format("%02d", from_hh) + "." + String.format("%02d", from_mm);
        long x = DateHelper.dateTimeToTimestamp(date);

        y = DateHelper.getCurrentTimestamp();

        if (y >= x) {
            Toast.makeText(context, "You are not allowed to book at this time, current time is greater than this time.", Toast.LENGTH_SHORT).show();
        } else {
            // Toast.makeText(context, "Welcome.", Toast.LENGTH_SHORT).show();
            TabLayoutModel tabLayoutModel = (TabLayoutModel) data.get(0);
            int selectedPosition = tabLayoutModel.selected;

            activity.displayBookDevice(dd, mm, yy, from_hh, from_mm, tabLayoutModel.resources, selectedPosition);
        }
    }

    @Override
    public void onCategoryResReceived(CategoryResModel model) {
        TabLayoutModel tabLayoutModel = new TabLayoutModel();
        tabLayoutModel.model = model;
        tabLayoutModel.tabs = new String[10];
        List<Resource> res = model.getItems().get(0).getResources();
        Collections.sort(res, Resource.getCompByName());
        tabLayoutModel.resources = res;
        int i, selected = 0, sequence = 100;
        for (i = 0; i < res.size(); i++) {
            tabLayoutModel.tabs[i] = res.get(i).getTitle();
            if (res.get(i).getSequence() < sequence) {
                sequence = res.get(i).getSequence();
                selected = i;
            }
        }
        selected = AppPref.getSelected(context, selected);
        tabLayoutModel.length = i;
        tabLayoutModel.selected = selected;
        tabLayoutModel.selectedId = res.get(selected).getId();
        data.add(0, tabLayoutModel);
        adapter.updateData(data);
        adapter.notifyItemChanged(0);
        setUpCalender();
    }

    @Override
    public void onBookListForResSuccess(ShowBookByResModel model) {
        List<Item> items = model.getItems();
        //int length = items.size();
        int length = 0;
        boolean allEmptyFlag = false;
        List<Schedule> schedules = null;
        if (items.size() == 0) {
            length = 0;
            allEmptyFlag = true;
        } else {
            length = items.get(0).getSchedule().size();
            schedules = items.get(0).getSchedule();
            Collections.sort(
                    schedules,
                    new Comparator<Schedule>() {
                        @Override
                        public int compare(Schedule s1, Schedule s2) {
                            return s1.getFrom().compareTo(s2.getFrom());
                        }
                    }
            );
            if (AppPref.getIsMyBooking(context, false)) {
                for (int i = 0; i < length; ) {
                    if (!schedules.get(i).getUserId().getId().equals(AppPref.getUserId(context))) {
                        schedules.remove(i);
                        length--;
                    } else {
                        i++;
                    }
                }
            }
        }

        int startHour = 0;
        int startMinute = 0;

        TabLayoutModel tlm = ((TabLayoutModel) data.get(0));
        int meetingRoomIndex = tlm.selected;
        String meetingroom = tlm.tabs[meetingRoomIndex];

        CalenderModel cm = ((CalenderModel) data.get(2));
        int d = cm.currDate;
        int m = cm.month + 1;
        int y = cm.year;

        long myTime = 0, theirTime = 0;
        long difference;
        Schedule schedule = null;
        ViewDetailsModel model1;

        SimpleModel model2 = new SimpleModel();
        data.add(model2);
        int x = 0;
        boolean prefBooked = false;

        while (startHour < 24) {
            myTime = DateHelper.dateTimeToTimestamp(y + "-" + m + "-" + d + "-" + startHour + "." + startMinute);
            if (x < length) {
                //schedule = items.get(0).getSchedule().get(x);
                schedule = schedules.get(x);
                theirTime = (long) Double.parseDouble(schedule.getFrom());
            } else {
                allEmptyFlag = true;
                theirTime = DateHelper.dateTimeToTimestamp(y + "-" + m + "-" + d + "-" + 23 + "." + 60);
            }
            if ((allEmptyFlag == true) || (myTime < theirTime)) {
                difference = (theirTime - myTime) / 60000;
                for (long freeTimes = 0; freeTimes < difference; freeTimes += 30) {
                    model1 = new ViewDetailsModel();
                    model1.isBooked = false;
                    model1.yy = y;
                    model1.mm = m;
                    model1.dd = d;

                    if (startHour > 12) {
                        model1.time = String.format("%02d", (startHour - 12)) + ":" + String.format("%02d", startMinute) + " PM";
                    } else if (startHour < 12) {
                        if (startHour == 0) {
                            model1.time = "12:" + String.format("%02d", startMinute) + " AM";
                        } else
                            model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " AM";
                    } else {
                        model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " PM";
                    }
                    model1.from_hh = startHour;
                    model1.from_mm = startMinute;
                    if (model1.time.contains("30")) {
                        model1.time = "";
                        model1.blankTime = true;
                    }
                    model1.prevBooked = prefBooked;
                    prefBooked = false;
                    data.add(model1);

                    if (startMinute == 30) {
                        startHour++;
                        startMinute = 0;
                    } else {
                        startMinute += 30;
                    }
                }
            } else {
                difference = ((long) Double.parseDouble(schedule.getTo()) - theirTime) / 60000;
                for (long freeTimes = 0; freeTimes < difference; freeTimes += 30) {
                    if (freeTimes == 0) {
                        model1 = new ViewDetailsModel();
                        model1.schedule = schedule;
                        if (AppPref.getUserId(context).equals(schedule.getUserId().getId())) {
                            model1.isMine = true;
                        } else {
                            model1.isMine = false;
                        }
                        model1.yy = y;
                        model1.mm = m;
                        model1.dd = d;
                        if (startHour > 12) {
                            model1.time = String.format("%02d", (startHour - 12)) + ":" + String.format("%02d", startMinute) + " PM";
                        } else if (startHour < 12) {
                            if (startHour == 0) {
                                model1.time = "12:" + String.format("%02d", startMinute) + " AM";
                            } else
                                model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " AM";
                        } else {
                            model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " PM";
                        }
                        model1.from_hh = startHour;
                        model1.from_mm = startMinute;
                        model1.minutes = (int) difference;

                        model1.isBooked = true;
                        model1.isEmpty = false;
                        model1.meetingRoom = meetingroom;
                        model1.meetingRoomIndex = meetingRoomIndex;

                        model1.createdBy = schedule.getUserId().getFirstName();

                        model1.label = schedule.getTitle();
                        model1.description = (schedule.getDescription() != null) ? schedule.getDescription() : "Not defined";
                        if (model1.time.contains("30")) {
                            model1.time = "";
                            model1.blankTime = true;
                        }
                        model1.prevBooked = prefBooked;
                        prefBooked = true;
                        data.add(model1);
                        if (startMinute == 30) {
                            startHour++;
                            startMinute = 0;
                        } else {
                            startMinute += 30;
                        }
                    } else {
                        model1 = new ViewDetailsModel();
                        if (startHour > 12) {
                            model1.time = String.format("%02d", (startHour - 12)) + ":" + String.format("%02d", startMinute) + " PM";
                        } else if (startHour < 12) {
                            if (startHour == 0) {
                                model1.time = "12:" + String.format("%02d", startMinute) + " AM";
                            } else
                                model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " AM";
                        } else {
                            model1.time = String.format("%02d", (startHour)) + ":" + String.format("%02d", startMinute) + " PM";
                        }
                        model1.isBooked = true;
                        model1.isEmpty = true;
                        if (model1.time.contains("30")) {
                            model1.time = "";
                            model1.blankTime = true;
                        }
                        model1.prevBooked = prefBooked;
                        prefBooked = true;


                        model1.yy = y;
                        model1.mm = m;
                        model1.dd = d;
                        model1.from_hh = startHour;
                        model1.from_mm = startMinute;
                        model1.minutes = (int) difference;
                        model1.createdBy = schedule.getUserId().getFirstName();
                        model1.meetingRoom = meetingroom;
                        model1.label = schedule.getTitle();

                        data.add(model1);
                        if (startMinute == 30) {
                            startHour++;
                            startMinute = 0;
                        } else {
                            startMinute += 30;
                        }
                    }
                }
                x++;
            }
        }
        adapter.updateData(data);
        adapter.notifyDataSetChanged();
        this.isFullLoaded = true;
        // mRecyclerView.smoothScrollToPosition(20);
//        adapter.notifyItemChanged(3);
//        adapter.notifyItemChanged(4);
//        adapter.notifyItemChanged(5);
        //adapter.notifyItemRangeChanged(3, 49);
    }

    @Override
    public void onDeleteClick(final String id, final int startPosition, final int delTime) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Warning!");
        dialog.setMessage("This booking will be deleted permanenty. Are you sure you want to delete it?");
        dialog.setCancelable(false);
        dialog.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.setPositiveButton(
                "Delete it.",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.tryDeleteApi(id, startPosition, delTime, ShowBookingsFragment.this);
                        dialog.dismiss();
                    }
                }
        );
        dialog.show();
    }

    @Override
    public void onDeleteSuccess(int startPos, int numItems) {
        for (int i = 0; i < numItems; i++) {
            ViewDetailsModel mo = (ViewDetailsModel) data.get(startPos + i);
            mo.isBooked = false;
            data.remove(startPos + i);
            data.add(startPos + i, mo);
        }

        adapter.updateData(data);
        adapter.notifyDataSetChanged();
    }

    private void setUpCalender() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DATE);


        MonthModel monthModel = new MonthModel();
        monthModel.prevMonthText = AppConst.monthArray[month - 1];
        monthModel.nextMonthText = AppConst.monthArray[month + 1];
        monthModel.monthYearText = AppConst.monthArray[month] + ", " + year;
        monthModel.month = month;
        monthModel.year = year;
        data.add(monthModel);

        CalenderModel calenderModel = new CalenderModel();
        calenderModel.year = year;
        calenderModel.month = month;
        calenderModel.date = day;
        calenderModel.currDate = day;
        data.add(calenderModel);

        adapter.updateData(data);
        adapter.notifyItemChanged(1);
        adapter.notifyItemChanged(2);

        String from_date = String.format("%4d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", day);
        String resourceId = ((TabLayoutModel) this.data.get(0)).selectedId;
        presenter.sendViewBookingOnResource(from_date, from_date, resourceId, "0", "100", this);
        // initRecyclerView();
    }
//    private void initRecyclerView() {
//        Calendar now = Calendar.getInstance();
//        int year = now.get(Calendar.YEAR);
//        int month = now.get(Calendar.MONTH);
//        int day = now.get(Calendar.DATE);
//
//
//        ViewDetailsModel model1 = new ViewDetailsModel();
//        model1.time = "12 AM";
//        model1.label = "Vishu - Booking App Demo";
//        model1.description = "Booking App Demo";
//        model1.from_hh = 0;
//        model1.from_mm = 0;
//        model1.minutes = 300;
//        model1.isBooked = true;
//        model1.isEmpty = false;
//        model1.meetingRoom = "MR 3";
//        model1.meetingRoomIndex = 2;
//        model1.createdBy = "Vishu";
//        model1.yy = year;
//        model1.mm = month;
//        model1.dd = day;
//
//        data.add(model1);
//        model1 = new ViewDetailsModel();
//        model1.time = "12:30 AM";
//        model1.isBooked = true;
//        model1.isEmpty = true;
//        data.add(model1);
//
//        for (int i = 1; i < 6; i++) {
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":00 AM";
//            model1.isBooked = true;
//            model1.isEmpty = true;
//            data.add(model1);
//
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":30 AM";
//            model1.isBooked = true;
//            model1.isEmpty = true;
//            data.add(model1);
//        }
//        for (int i = 6; i < 12; i++) {
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":00 AM";
//            model1.isBooked = false;
//            model1.yy = year;
//            model1.mm = month;
//            model1.dd = day;
//            model1.from_hh = i;
//            model1.from_mm = 0;
//            data.add(model1);
//
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":30 AM";
//            model1.isBooked = false;
//            model1.yy = year;
//            model1.mm = month;
//            model1.dd = day;
//            model1.from_hh = i;
//            model1.from_mm = 30;
//            data.add(model1);
//        }
//        model1 = new ViewDetailsModel();
//        model1.time = "12 PM";
//        model1.label = "Himanshu - Booking App Demo";
//        model1.description = "Booking App Demo";
//        model1.from_hh = 12;
//        model1.from_mm = 0;
//        model1.minutes = 300;
//        model1.isBooked = true;
//        model1.isEmpty = false;
//        model1.meetingRoom = "MR 3";
//        model1.meetingRoomIndex = 2;
//        model1.createdBy = "Himanshu";
//        model1.yy = year;
//        model1.mm = month;
//        model1.dd = day;
//
//        data.add(model1);
//
//        model1 = new ViewDetailsModel();
//        model1.time = "12:30 PM";
//        model1.isBooked = true;
//        model1.isEmpty = true;
//        data.add(model1);
//
//        for (int i = 1; i < 6; i++) {
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":00 PM";
//            model1.isBooked = true;
//            model1.isEmpty = true;
//            data.add(model1);
//
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":30 PM";
//            model1.isBooked = true;
//            model1.isEmpty = true;
//            data.add(model1);
//        }
//        for (int i = 6; i < 12; i++) {
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":00 PM";
//            model1.isBooked = false;
//            model1.yy = year;
//            model1.mm = month;
//            model1.dd = day;
//            model1.from_hh = i + 12;
//            model1.from_mm = 0;
//            data.add(model1);
//
//            model1 = new ViewDetailsModel();
//            model1.time = (i) + ":30 PM";
//            model1.isBooked = false;
//            model1.yy = year;
//            model1.mm = month;
//            model1.dd = day;
//            model1.from_hh = i + 12;
//            model1.from_mm = 30;
//            data.add(model1);
//        }
//        adapter = new ShowBookingAdapter(data, context, this);
//        mRecyclerView.setAdapter(adapter);
//
//    }

    private void startRecyclerView() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setEnabled(true);
        mRecyclerView.setScrollContainer(true);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mLayoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // data = new ArrayList<>();
        adapter = new ShowBookingAdapter(data, context, this);
        mRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (isFullLoaded) {
            outState.putSerializable("TheMainModel", data);
        }
    }

    public void onCalClick() {
        mRecyclerView.smoothScrollToPosition(2);
    }
}
