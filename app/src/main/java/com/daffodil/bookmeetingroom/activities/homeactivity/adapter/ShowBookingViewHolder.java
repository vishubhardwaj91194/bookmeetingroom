package com.daffodil.bookmeetingroom.activities.homeactivity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewCalender;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewData;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewDetails;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewMonth;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewTabLayout;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.adapter . ShowBookingViewHolder
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ShowBookingViewHolder extends RecyclerView.ViewHolder {
    public ViewTabLayout viewTabLayout;
    public ViewMonth viewMonth;
    public ViewCalender viewCalender;
    public ViewData viewData;
    public ViewDetails viewDetails;

    public ShowBookingViewHolder(View itemView) {
        super(itemView);

        if (itemView instanceof ViewTabLayout) {
            this.viewTabLayout = (ViewTabLayout) itemView;
        } else if (itemView instanceof ViewMonth) {
            this.viewMonth = (ViewMonth) itemView;
        } else if (itemView instanceof ViewCalender) {
            this.viewCalender = (ViewCalender) itemView;
        } else if (itemView instanceof ViewData) {
            this.viewData = (ViewData) itemView;
        } else if (itemView instanceof ViewDetails) {
            this.viewDetails = (ViewDetails) itemView;
        }
    }
}
