package com.daffodil.bookmeetingroom.activities.mainactivity.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.model . GmailDataModel
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class GmailDataModel implements Parcelable {
    public String id;
    public Uri photoUri;
    public String email;
    public String name;

    public GmailDataModel(Parcel in) {
        id = in.readString();
        photoUri = in.readParcelable(Uri.class.getClassLoader());
        email = in.readString();
        name = in.readString();
    }

    public static final Creator<GmailDataModel> CREATOR = new Creator<GmailDataModel>() {
        @Override
        public GmailDataModel createFromParcel(Parcel in) {
            return new GmailDataModel(in);
        }

        @Override
        public GmailDataModel[] newArray(int size) {
            return new GmailDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(photoUri, flags);
        dest.writeString(email);
        dest.writeString(name);
    }
}
