package com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.model . Resource
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class Resource implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("sequence")
    @Expose
    private int sequence;
    @SerializedName("availability")
    @Expose
    private boolean availability;

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The _id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The sequence
     */
    public int getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     * The sequence
     */
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     * The availability
     */
    public boolean isAvailability() {
        return availability;
    }

    /**
     *
     * @param availability
     * The availability
     */
    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public static Comparator<Resource> getCompByName() {
        Comparator comp = new Comparator<Resource>(){
            @Override
            public int compare(Resource s1, Resource s2)
            {
                return s1.getTitle().compareTo(s2.getTitle());
            }
        };
        return comp;
    }

}
