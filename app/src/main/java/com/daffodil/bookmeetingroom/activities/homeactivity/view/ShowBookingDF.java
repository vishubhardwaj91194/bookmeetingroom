package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ShowBookingDF
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ShowBookingDF extends DialogFragment implements View.OnClickListener {
    private static ShowBookingDF instance = null;
    private Context context;
    private ViewDetailsModel model;
    private View view;

    public ShowBookingDF() {}
    public static ShowBookingDF getInstance() {
        if(instance ==null) {
            instance = new ShowBookingDF();
        }
        return instance;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setModel(ViewDetailsModel model) {
        this.model = model;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.display_booking, container);
        this.view = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView title = (TextView) view.findViewById(R.id.tvTitle);
        TextView createdBy = (TextView) view.findViewById(R.id.tvCreatedBy);
        TextView location = (TextView) view.findViewById(R.id.tvLocation);
        TextView schedule = (TextView) view.findViewById(R.id.tvSchedule);
        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);

        if (model == null) {
            this.model = (ViewDetailsModel) savedInstanceState.getSerializable("model");
        }
        title.setText(model.label);
        location.setText(model.meetingRoom);
        String scheduleText = String.format("%02d",model.mm) + "/" + String.format("%02d",model.dd) + "/" + String.format("%02d",model.yy) + "(" + String.format("%02d",model.from_hh) + ":" +String.format("%02d",model.from_mm) + " - " + String.format("%02d",model.from_hh + model.minutes/60) + ":" +String.format("%02d",model.from_mm + model.minutes%60) + ")";
        schedule.setText(scheduleText);
        createdBy.setText(model.createdBy);

    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("model", model);
    }
}
