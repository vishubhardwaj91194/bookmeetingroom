package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.TabLayoutModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ViewTabLayout
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewTabLayout extends RelativeLayout implements TabLayout.OnTabSelectedListener {
    private Context context;
    private TabLayout tabLayout;
    private TabLayoutModel model;
    private iShowBooking clickListener;
    private View view;

    public ViewTabLayout(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.show_book_tab, this, true);
        this.view = view;
        RelativeLayout viewRoot = (RelativeLayout) view;
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setBackgroundColor(Color.GRAY);
        tabLayout.setTabTextColors(context.getResources().getColor(R.color.colorPrimary), context.getResources().getColor(R.color.colorPrimary));
        tabLayout.setSelectedTabIndicatorColor(context.getResources().getColor(R.color.colorPrimary));
    }

    public void setData(TabLayoutModel model) {
        tabLayout.removeAllTabs();
        this.model = model;
        for(int i=0; i<model.length; i++) {
            if(i == model.selected) {
                tabLayout.addTab(tabLayout.newTab().setText(model.tabs[i]), true);
            } else {
                tabLayout.addTab(tabLayout.newTab().setText(model.tabs[i]), false);
            }
        }
        tabLayout.setOnTabSelectedListener(this);
    }

    public void setClickListener(iShowBooking listener) {
        this.clickListener = listener;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (model.selected != tab.getPosition()) {
            clickListener.onTabChanged(tab.getPosition());
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
