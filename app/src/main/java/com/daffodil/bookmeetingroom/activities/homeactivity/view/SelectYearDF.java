package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.daffodil.bookmeetingroom.R;

import java.util.ArrayList;
import java.util.List;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . SelectYearDF
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class SelectYearDF extends DialogFragment implements View.OnClickListener {
    private static SelectYearDF instance = null;
    private iShowBooking listener;
    private int year;
    private Spinner spinner;
    private Context context;

    public SelectYearDF() {

    }
    public static SelectYearDF getInstance() {
        if(instance == null) {
            instance = new SelectYearDF();
        }
        return instance;
    }
    public void setListener(iShowBooking listener) {
        this.listener = listener;
    }
    public void setCurrentYear(int year) {
        this.year = year;
    }
    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.select_year, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.spinner);

        List<String> years = new ArrayList<String>();
        for(int i=2010; i<=year+2; i++) {
            years.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, R.id.spinnerTextView, years);
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(year-2010);

        Button cancel = (Button) view.findViewById(R.id.btnCancel);
        Button done = (Button) view.findViewById(R.id.btnDone);
        cancel.setOnClickListener(this);
        done.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnCancel) {
            dismiss();
        } else if(v.getId() == R.id.btnDone) {
            int year = Integer.parseInt(spinner.getSelectedItem().toString());
            listener.onYearChange(year);
            dismiss();
        }
    }
}
