package com.daffodil.bookmeetingroom.activities.homeactivity.model;

import java.io.Serializable;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.model . CalenderModel
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class CalenderModel extends ShowBookingBaseModel implements Serializable {
    public int year, month, date;
    public int currDate;
}
