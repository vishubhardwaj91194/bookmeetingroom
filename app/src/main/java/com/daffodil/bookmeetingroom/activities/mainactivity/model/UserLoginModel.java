package com.daffodil.bookmeetingroom.activities.mainactivity.model;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.model . UserLoginResponceListener
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class UserLoginModel {
    public Auth auth;
    public User user;
}

