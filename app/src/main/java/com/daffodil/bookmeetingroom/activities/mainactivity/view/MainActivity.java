package com.daffodil.bookmeetingroom.activities.mainactivity.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.HomeActivity;
import com.daffodil.bookmeetingroom.activities.mainactivity.model.GmailDataModel;
import com.daffodil.bookmeetingroom.activities.mainactivity.model.UserLoginModel;
import com.daffodil.bookmeetingroom.activities.mainactivity.presenter.MainPresenter;
import com.daffodil.bookmeetingroom.helpers.AppConst;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, iMainActivity {

    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 1;
    private ProgressDialog progressDialog;
    private MainPresenter presenter;
    private String id, name, email;
    private Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this, this);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setColorScheme(SignInButton.COLOR_DARK);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setOnClickListener(this);

        if (AppPref.getGoogleLoginID(this) != null) {
            Log.d(AppConst.TAG, "connected");
            signIn();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }

    }

    private void signIn() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait!");
        progressDialog.setMessage("Trying to login with google.");
        progressDialog.show();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(AppConst.TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            id = acct.getId();
            photoUri = acct.getPhotoUrl();
            name = acct.getDisplayName();
            email = acct.getEmail();

            if (email.endsWith("daffodilsw.com")) {
                presenter.tryUserLoginApi(name, email, id);
            } else {
                Toast.makeText(this, "Sorry bro, this app can only be accessed by daffodilsw email account.", Toast.LENGTH_SHORT).show();
                signOut();
            }

            //signOut();

            // signOut();
            // mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            // updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
            Toast.makeText(this, "Google login failed", Toast.LENGTH_SHORT).show();
            signOut();
        }
    }

    @Override
    public void onUserLoginSuccess(UserLoginModel passModel) {
        Parcel parcel = Parcel.obtain();
        parcel.writeString(id);
        parcel.writeParcelable(photoUri, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
        parcel.writeString(email);
        parcel.writeString(name);

        GmailDataModel model = new GmailDataModel(parcel);

        model.id = id;
        model.photoUri = photoUri;
        model.name = name;
        model.email = email;

        AppPref.setGoogleLoginId(model.id, this);

        Intent nextActivity = new Intent();
        nextActivity.setClass(this, HomeActivity.class);
        nextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConst.LOGIN_MODEL_KEY, model);
        nextActivity.putExtras(bundle);

        //nextActivity.putExtra(AppConst.LOGIN_MODEL_KEY, model);
        startActivity(nextActivity);
        finish();
    }

    @Override
    public void onUserLoginFailure() {
        signOut();
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        //updateUI(false);
                        AppPref.setGoogleLoginId(null, MainActivity.this);
                    }
                });
    }

}
