package com.daffodil.bookmeetingroom.activities.homeactivity.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.CategoryResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.ShowBookByResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.singlebooking.SingleBookingError;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.BookDeviceFragment;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.iBookDeviceFragment;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.iShowBooking;
import com.daffodil.bookmeetingroom.helpers.ApiManager;
import com.daffodil.bookmeetingroom.helpers.AppConst;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.daffodil.bookmeetingroom.helpers.HeaderManager;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.presenter . HomePresenter
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class HomePresenter implements iHomePresenter {

    private Context context;
    private ProgressDialog progressDialog;
    private iShowBooking showBookingListener;
    private iBookDeviceFragment frags;

    public HomePresenter(Context context) {
        this.context = context;
    }

    public void tryLoadCategories(iShowBooking listener) {
        this.showBookingListener = listener;
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait.");
        progressDialog.setMessage("Please wait while loading categories");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        Log.d(AppConst.TAG, "Token "+AppPref.getToken(context));
        Map<String, String> header = HeaderManager.getComplexMap(
                AppPref.getToken(context)
        );
        String url = AppConst.BASE_URL + "/api/categories";
        ApiManager.sendCategoriesApi(url, header, this, context);
    }

    @Override
    public void onLoadCategoriesSuccess(String response) {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Gson gson = new Gson();
        CategoryResModel model = gson.fromJson(response, CategoryResModel.class);
        showBookingListener.onCategoryResReceived(model);

    }

    @Override
    public void onBookingListForResource(String response) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Gson gson = new Gson();
        ShowBookByResModel model = gson.fromJson(response, ShowBookByResModel.class);
        showBookingListener.onBookListForResSuccess(model);
    }

    public void onDeleteSuccess(String response, int startPos, int numItems) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        showBookingListener.onDeleteSuccess(startPos, numItems);
    }

    @Override
    public void onSingleBookingError(String s) {
        Gson gson = new Gson();
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        try {
            SingleBookingError error = gson.fromJson(s, SingleBookingError.class);
            dialog.setTitle(error.getErrorCode());
            dialog.setMessage(error.getDescription());
        }catch (JsonSyntaxException e) {
            dialog.setTitle("Error !");
            dialog.setMessage("Some unknown error has occured.");
        }
        dialog.show();
        frags.error();
    }

    @Override
    public void onSingleBookingSuccess(String response) {
        Toast.makeText(context, "Congratulations, you have successfully booked the meeting room.", Toast.LENGTH_SHORT).show();
        frags.error();

    }

    @Override
    public void onMultipleBookingSuccess(String response) {
        Toast.makeText(context, "Congratulations, you have successfully booked the meeting room.", Toast.LENGTH_SHORT).show();
        frags.error();
    }

    @Override
    public void onMultipleBookingError(String s) {
        Gson gson = new Gson();
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        try {
            SingleBookingError error = gson.fromJson(s, SingleBookingError.class);
            dialog.setTitle(error.getErrorCode());
            dialog.setMessage(error.getDescription());
        }catch (JsonSyntaxException e) {
            dialog.setTitle("Error !");
            dialog.setMessage("Some unknown error has occured.");
        }
        dialog.show();
        frags.error();
    }

    public void sendViewBookingOnResource(String from, String to, String resourceId, String skip, String limit, iShowBooking listener) {
        this.showBookingListener = listener;
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait.");
        progressDialog.setMessage("Please wait while loading categories");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        JSONObject body = new JSONObject();
        try {
            body.put(AppConst.BODY_KEY_4, from);
            body.put(AppConst.BODY_KEY_5, to);
            body.put(AppConst.BODY_KEY_6, resourceId);
            body.put(AppConst.BODY_KEY_7, skip);
            body.put(AppConst.BODY_KEY_8, limit);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> header = HeaderManager.getComplexMap(
                AppPref.getToken(context)
        );

        String url = AppConst.BASE_URL + "/api/resource/bookingList";
        ApiManager.sendBookingListForResource(url, header, body, this, context);
    }

    public void tryDeleteApi(String id, int startPos, int numItem, iShowBooking listener) {
        this.showBookingListener = listener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait.");
        progressDialog.setMessage("Please wait while loading categories");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        String url = AppConst.BASE_URL + "/api/booking/schedule/" + id;
        Map <String, String> header = HeaderManager.getComplexMap(AppPref.getToken(context));
        ApiManager.sendDEleteBookingApi(url, header, this, context, startPos, numItem);
    }

    public void trySingleBooking(String userId, String resourceId, String date, String fromTimestamp, String toTimestamp, String title, String description, iBookDeviceFragment listener) {
        this.frags = listener;
        String url = AppConst.BASE_URL + "/api/user/" + userId + "/book/resource/" + resourceId;
        Map<String, String> header = HeaderManager.getComplexMap(
                AppPref.getToken(context)
        );
        JSONObject body = new JSONObject();
        try {
            body.put(AppConst.BODY_KEY_9, date);
            JSONObject schedule = new JSONObject();
            schedule.put(AppConst.BODY_KEY_11, fromTimestamp);
            schedule.put(AppConst.BODY_KEY_12, toTimestamp);
            schedule.put(AppConst.BODY_KEY_13, title);
            schedule.put(AppConst.BODY_KEY_14, description);
            body.put(AppConst.BODY_KEY_10, schedule);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.sendBookSingleDevice(url, header, body, context, this);
    }

    public void tryMultipleBooking(String userId, String resourceId, String fromDate, String toDate, String fromTimestamp, String toTimestamp, String title, String description, BookDeviceFragment listener) {
        this.frags = listener;
        String url = AppConst.BASE_URL + "/api/user/" + userId + "/book/resource/" + resourceId + "/repeat";
        Map<String, String> header = HeaderManager.getComplexMap(
                AppPref.getToken(context)
        );
        JSONObject body = new JSONObject();
        try {
            body.put(AppConst.BODY_KEY_15, fromDate);
            body.put(AppConst.BODY_KEY_16, toDate);
            JSONObject schedule = new JSONObject();
            schedule.put(AppConst.BODY_KEY_11, fromTimestamp);
            schedule.put(AppConst.BODY_KEY_12, toTimestamp);
            schedule.put(AppConst.BODY_KEY_13, title);
            schedule.put(AppConst.BODY_KEY_14, description);
            body.put(AppConst.BODY_KEY_10, schedule);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.sendBookMultipleDevice(url, header, body, context, this);
    }


}
