package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.Resource;

import java.util.List;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . iHomePresenter
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iHomeActivity {
    public void displayEditBook(ViewDetailsModel model);
    public void onEditBookingCancel();

    void displayBookDevice(int dd, int mm, int yy, int from_hh, int from_mm, List<Resource> resources, int position);
}
