package com.daffodil.bookmeetingroom.activities.mainactivity.model;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.model . User
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class User {
    public String fullName, firstName, email, _id, createdAt;
    public boolean isAdmin, accountEnabled;
}
