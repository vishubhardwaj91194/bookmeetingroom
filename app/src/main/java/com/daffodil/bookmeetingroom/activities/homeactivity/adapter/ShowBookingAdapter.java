package com.daffodil.bookmeetingroom.activities.homeactivity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.CalenderModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.MonthModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ShowBookingBaseModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.TabLayoutModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewCalender;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewData;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewDetails;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewMonth;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.ViewTabLayout;
import com.daffodil.bookmeetingroom.activities.homeactivity.view.iShowBooking;
import com.daffodil.bookmeetingroom.helpers.AppConst;

import java.util.ArrayList;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity . ShowBookingAdapter
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ShowBookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ShowBookingBaseModel> data;
    private int length;
    private Context context;
    private iShowBooking listener;


    public ShowBookingAdapter(ArrayList<ShowBookingBaseModel> data, Context context, iShowBooking listener) {
        this.data = data;
        this.length = this.data.size();
        this.context = context;
        this.listener = listener;
    }

    public void updateData(ArrayList<ShowBookingBaseModel> data) {
        this.data = data;
        this.length = this.data.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ShowBookingViewHolder holder = null;
        if (viewType == AppConst.VIEW_TYPE_TAB_LAYOUT) {
            holder = new ShowBookingViewHolder(new ViewTabLayout(context));
        } else if (viewType == AppConst.VIEW_TYPE_MONTH) {
            holder = new ShowBookingViewHolder(new ViewMonth(context));
        } else if (viewType == AppConst.VIEW_TYPE_CALENDER) {
            holder = new ShowBookingViewHolder(new ViewCalender(context));
        } else if (viewType == AppConst.VIEW_TYPE_DATA) {
            holder = new ShowBookingViewHolder(new ViewData(context));
        } else if (viewType == AppConst.VIEW_TYPE_DETAILS) {
            holder = new ShowBookingViewHolder(new ViewDetails(context));
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ShowBookingViewHolder holder1 = (ShowBookingViewHolder) holder;
        int viewType = getItemViewType(position);
        if (viewType == AppConst.VIEW_TYPE_TAB_LAYOUT) {
            holder1.viewTabLayout.setData((TabLayoutModel)  data.get(position));
            holder1.viewTabLayout.setClickListener(listener);
        } else if (viewType ==AppConst.VIEW_TYPE_MONTH) {
            holder1.viewMonth.setData((MonthModel) data.get(position));
            holder1.viewMonth.setClickListener(listener);
        } else if (viewType == AppConst.VIEW_TYPE_CALENDER) {
            holder1.viewCalender.setData((CalenderModel) data.get(position));
            holder1.viewCalender.setClickListener(listener);
        } else if (viewType == AppConst.VIEW_TYPE_DATA) {

        } else if (viewType == AppConst.VIEW_TYPE_DETAILS) {
            holder1.viewDetails.setData((ViewDetailsModel) data.get(position), position-4);
            holder1.viewDetails.setClickListener(listener);
        }

    }

    @Override
    public int getItemCount() {
        return this.length;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) {
            return AppConst.VIEW_TYPE_TAB_LAYOUT;
        } else if(position == 1) {
            return AppConst.VIEW_TYPE_MONTH;
        } else if(position == 2) {
            return AppConst.VIEW_TYPE_CALENDER;
        } else if(position == 3) {
            return AppConst.VIEW_TYPE_DATA;
        } else {
            return AppConst.VIEW_TYPE_DETAILS;
        }
    }
}
