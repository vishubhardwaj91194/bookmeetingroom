package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ViewDetails
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewDetails extends RelativeLayout implements View.OnClickListener {

    private ViewDetailsModel model;
    private int position;
    private iShowBooking clickListener;
    private Context context;
    private TextView time, title;
    private ImageView btnEdit;
    private RelativeLayout viewRoot;
    LinearLayout box1, box2, box3, box4, mar1, mar2;

    public ViewDetails(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflator = LayoutInflater.from(context);
        // View view = inflator.inflate(R.layout.show_book_view_details, this, true);
        View view = inflator.inflate(R.layout.new_details_layout, this, true);
        viewRoot = (RelativeLayout) view;
        viewRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewDetails.this.model.isBooked == true) {
                    clickListener.showBooking(model, position, model);
                } else {
                    clickListener.showBookDevice(model.dd, model.mm, model.yy, model.from_hh, model.from_mm);
                }
            }
        });

        time = (TextView) view.findViewById(R.id.time);
        title = (TextView) view.findViewById(R.id.data);
        btnEdit = (ImageView) view.findViewById(R.id.btnEdit);

        title.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        box1 = (LinearLayout) findViewById(R.id.box1);
        box2 = (LinearLayout) findViewById(R.id.box2);
        box3 = (LinearLayout) findViewById(R.id.box3);
        box4 = (LinearLayout) findViewById(R.id.box4);
        mar1 = (LinearLayout) findViewById(R.id.mar1);
        mar2 = (LinearLayout) findViewById(R.id.mar2);
    }

    public void setData(ViewDetailsModel model, int position) {
        this.model = model;
        this.position = position;


        if (model.isBooked && model.isEmpty) {
            //viewRoot.setBackgroundColor(Color.GRAY);
            time.setText(model.time);
            //time.setTextColor(Color.WHITE);
            title.setText("");
            btnEdit.setVisibility(INVISIBLE);
            box4.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
            mar2.setVisibility(VISIBLE);
        } else if (model.isBooked && !model.isEmpty) {
            if (!model.isMine) {
                btnEdit.setVisibility(INVISIBLE);
            } else {
                btnEdit.setVisibility(VISIBLE);
            }
            //viewRoot.setBackgroundColor(Color.GRAY);
            time.setText(model.time);
            //time.setTextColor(Color.WHITE);
            title.setText(model.label);
            //title.setTextColor(Color.WHITE);
            box4.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
            mar2.setVisibility(VISIBLE);
        } else {
            viewRoot.setBackgroundColor(Color.TRANSPARENT);
            time.setTextColor(Color.BLACK);
            title.setTextColor(Color.BLACK);
            time.setText(model.time);
            title.setText("Free - Book Now");
            btnEdit.setVisibility(INVISIBLE);
            box4.setBackgroundColor(Color.TRANSPARENT);
            mar2.setVisibility(INVISIBLE);
        }

//        if (model.time.equals("")) {
//            time.setText("00:00 AM");
//            time.setVisibility(INVISIBLE);
//        }




        if (model.prevBooked) {
            box2.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
            //if (model.isBooked) {
                //box3.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
            //} else {
                box3.setBackgroundColor(Color.GRAY);
                mar1.setVisibility(VISIBLE);
            //}

        } else {
            box2.setBackgroundColor(Color.TRANSPARENT);
            box3.setBackgroundColor(Color.GRAY);
            mar1.setVisibility(INVISIBLE);
        }

        if (model.blankTime) {
            time.setText("00:00 AM");
            time.setVisibility(INVISIBLE);
            if (model.isBooked) {
                box3.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
                mar1.setVisibility(VISIBLE);
            } else {
                box3.setBackgroundColor(Color.TRANSPARENT);
                mar1.setVisibility(INVISIBLE);
            }
        } else if (model.prevBooked && model.isBooked) {
            time.setVisibility(VISIBLE);
            mar1.setVisibility(VISIBLE);
        } else {
            time.setVisibility(VISIBLE);
        }

        if (model.prevBooked && !model.isBooked) {
            mar1.setVisibility(VISIBLE);
            mar2.setVisibility(INVISIBLE);
        } else if (!model.prevBooked && model.isBooked) {
            mar1.setVisibility(INVISIBLE);
            mar2.setVisibility(VISIBLE);
        }

    }

    public void setClickListener(iShowBooking listener) {
        this.clickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnEdit) {
            if (this.model.isBooked == true && this.model.isEmpty == false) {
                // clickListener.onEditClick(this.model);
                int startPosition = this.position + 4;
                // int time = (int) ((Long.parseLong(this.model.schedule.getTo()) - Long.parseLong(this.model.schedule.getFrom()))/60000);
                int delTime = (model.minutes / 30);
                clickListener.onDeleteClick(this.model.schedule.getId(), startPosition, delTime);
            }
        } else if (v.getId() == R.id.data) {
            if (this.model.isBooked == true/* && this.model.isEmpty == false*/) {
                clickListener.showBooking(model, position, model);
            } else {
                clickListener.showBookDevice(model.dd, model.mm, model.yy, model.from_hh, model.from_mm);
            }
        }
    }
}
