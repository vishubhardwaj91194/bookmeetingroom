package com.daffodil.bookmeetingroom.activities.homeactivity.view;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . iBookDeviceFragment
 * Created by Vishu Bhardwaj on 21/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iBookDeviceFragment {
    void error();
}
