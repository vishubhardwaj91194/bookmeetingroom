package com.daffodil.bookmeetingroom.activities.homeactivity.model.singlebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.model.singlebooking . SingleBookingError
 * Created by Vishu Bhardwaj on 21/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class SingleBookingError {

    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("details")
    @Expose
    private Object details;

    /**
     *
     * @return
     * The errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The errorCode
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The details
     */
    public Object getDetails() {
        return details;
    }

    /**
     *
     * @param details
     * The details
     */
    public void setDetails(Object details) {
        this.details = details;
    }

}