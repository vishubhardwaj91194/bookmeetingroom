package com.daffodil.bookmeetingroom.activities.mainactivity.presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.daffodil.bookmeetingroom.activities.mainactivity.model.UserLoginModel;
import com.daffodil.bookmeetingroom.activities.mainactivity.view.iMainActivity;
import com.daffodil.bookmeetingroom.helpers.ApiManager;
import com.daffodil.bookmeetingroom.helpers.AppConst;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.daffodil.bookmeetingroom.helpers.HeaderManager;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.presenter . MainPresenter
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class MainPresenter implements iMainPresenter {

    private Context context;
    private iMainActivity activity;
    private ProgressDialog progressDialog;

    public MainPresenter(Context context, iMainActivity activity) {
        this.context = context;
        this.activity = activity;
    }

    public void tryUserLoginApi(String name, String email, String googlePlusId) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Please wait while calling user login api.");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        JSONObject body = new JSONObject();
        try {
            body.put(AppConst.BODY_KEY_1, email);
            body.put(AppConst.BODY_KEY_2, googlePlusId);
            body.put(AppConst.BODY_KEY_3, name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Map<String, String> header = HeaderManager.getSimpleMap();
        String url = AppConst.BASE_URL+"/api/user/google/login";
        ApiManager.sendUserLoginApi(url, header, body, this, context);
    }

    @Override
    public void onUserLoginSuccess(String responce) {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Gson gson = new Gson();
        UserLoginModel model = gson.fromJson(responce, UserLoginModel.class);
        AppPref.setUserId(model.auth.userId, context);
        AppPref.setToken(model.auth.token, context);
        activity.onUserLoginSuccess(model);
    }
}
