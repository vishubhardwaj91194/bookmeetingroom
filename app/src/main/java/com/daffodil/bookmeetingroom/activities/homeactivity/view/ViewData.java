package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.daffodil.bookmeetingroom.R;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ViewData
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewData extends RelativeLayout {
    private Context context;

    public ViewData(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.show_book_view_data, this, true);
        RelativeLayout viewRoot = (RelativeLayout) view;
    }
}
