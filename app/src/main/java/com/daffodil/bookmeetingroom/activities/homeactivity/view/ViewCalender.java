package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.CalenderModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.util.Calendar;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ViewCalender
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewCalender extends RelativeLayout implements OnMonthChangedListener, OnDateSelectedListener {
    private Context context;
    private MaterialCalendarView mcv;
    private CalenderModel data;
    private iShowBooking clickListener;

    public ViewCalender(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.show_book_calender, this, true);
        RelativeLayout viewRoot = (RelativeLayout) view;

        mcv = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        mcv.state().edit()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setMinimumDate(CalendarDay.from(2010, 1, 1))
                .setMaximumDate(CalendarDay.from(20130, 12, 31))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        mcv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
        mcv.setShowOtherDates(MaterialCalendarView.SHOW_DECORATED_DISABLED);
        mcv.setAllowClickDaysOutsideCurrentMonth(false);
        mcv.setTopbarVisible(false);
        mcv.setOnMonthChangedListener(this);
        mcv.setOnDateChangedListener(this);
    }

    public void setData(CalenderModel model) {
        this.data = model;
        mcv.setCurrentDate(CalendarDay.from(model.year, model.month, model.currDate));
        mcv.setSelectedDate(CalendarDay.from(model.year, model.month, model.currDate));
    }

    public void setClickListener(iShowBooking listener) {
        this.clickListener = listener;
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = 1;
        if (data.year == date.getYear() && data.month == date.getMonth() && data.date == date.getDay()) {

        } else {
            clickListener.onChangeMonth(year, month, day);
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        int day = date.getDay();
        if (data.date != day || data.currDate != day) {
            clickListener.changeDate(day);
        }
    }
}
