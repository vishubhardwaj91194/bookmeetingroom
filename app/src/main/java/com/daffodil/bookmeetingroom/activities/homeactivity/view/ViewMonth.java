package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.MonthModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . ViewMonth
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewMonth extends RelativeLayout implements View.OnClickListener {
    private Context context;
    private MonthModel model;
    private TextView prevMonth, nextMonth, monthYear;
    private iShowBooking clickListener;

    public ViewMonth(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.show_book_month, this, true);
        RelativeLayout viewRoot = (RelativeLayout) view;

        this.prevMonth = (TextView) view.findViewById(R.id.prevMonth);
        this.nextMonth = (TextView) view.findViewById(R.id.nextMonth);
        this.monthYear = (TextView) view.findViewById(R.id.monthYear);

        prevMonth.setOnClickListener(this);
        nextMonth.setOnClickListener(this);
        monthYear.setOnClickListener(this);
    }

    public void setData(MonthModel data) {
        this.model = data;
        prevMonth.setText(model.prevMonthText);
        nextMonth.setText(model.nextMonthText);
        monthYear.setText(model.monthYearText);
    }

    public void setClickListener(iShowBooking listener) {
        this.clickListener = listener;
    }

    @Override
    public void onClick(View v) {
        int month = model.month;
        int year = model.year;
        int newMonth, newYear;
        switch (v.getId()) {
            case R.id.prevMonth:
                if (month == 0) {
                    newMonth = 11;
                    newYear = year - 1;
                } else {
                    newMonth = month - 1;
                    newYear = year;
                }
                clickListener.onChangeMonth(newYear, newMonth, 1);
                break;
            case R.id.nextMonth:
                if(month == 11) {
                    newMonth = 0;
                    newYear = year+1;
                } else {
                    newMonth = month+1;
                    newYear = year;
                }
                clickListener.onChangeMonth(newYear, newMonth, 1);
                break;
            case R.id.monthYear:
                clickListener.showYearSpinner();
                break;
            default:
                break;
        }
    }
}
