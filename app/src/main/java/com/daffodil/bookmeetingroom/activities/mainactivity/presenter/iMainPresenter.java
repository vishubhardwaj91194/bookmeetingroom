package com.daffodil.bookmeetingroom.activities.mainactivity.presenter;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.presenter . iMainPresenter
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iMainPresenter {
    public void onUserLoginSuccess(String responce);

}
