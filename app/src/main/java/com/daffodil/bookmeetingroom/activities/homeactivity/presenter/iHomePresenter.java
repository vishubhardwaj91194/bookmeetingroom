package com.daffodil.bookmeetingroom.activities.homeactivity.presenter;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.presenter . iHomePresenter
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iHomePresenter {
    public void onLoadCategoriesSuccess(String response);

    public void onBookingListForResource(String response);

    void onDeleteSuccess(String response, int startPos, int numItems);

    void onSingleBookingError(String s);

    void onSingleBookingSuccess(String response);

    void onMultipleBookingSuccess(String response);

    void onMultipleBookingError(String s);
}
