package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.CategoryResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.ShowBookByResModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . iShowBooking
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iShowBooking {
    public void onYearChange(int year);

    public void onChangeMonth(int year, int month, int date);

    public void onTabChanged(int position);

    public void showYearSpinner();

    public void changeDate(int day);

    public void onEditClick(ViewDetailsModel model);

    public void showBooking(ViewDetailsModel model, int position, ViewDetailsModel viewDetailsModel);

    void showBookDevice(int dd, int mm, int yy, int from_hh, int from_mm);

    void onCategoryResReceived(CategoryResModel model);

    void onBookListForResSuccess(ShowBookByResModel model);

    void onDeleteClick(String id, int startPosition, int delTime);

    void onDeleteSuccess(int startPos, int numItems);
}
