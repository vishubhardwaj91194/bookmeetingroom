package com.daffodil.bookmeetingroom.activities.mainactivity.view;

import com.daffodil.bookmeetingroom.activities.mainactivity.model.UserLoginModel;

/**
 * com.daffodil.bookmeetingroom.activities.mainactivity.view . iMainActivity
 * Created by Vishu Bhardwaj on 19/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public interface iMainActivity {
    public void onUserLoginSuccess(UserLoginModel model);
    public void onUserLoginFailure();
}
