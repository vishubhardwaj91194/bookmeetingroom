package com.daffodil.bookmeetingroom.activities.homeactivity.model;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.CategoryResModel;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.Resource;

import java.io.Serializable;
import java.util.List;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.model . TabLayoutModel
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class TabLayoutModel extends ShowBookingBaseModel implements Serializable {
    public String[] tabs = {"MR 1", "MR 2", "MR 3", "MR 4", "MR 5", "MR 6", "MR 7", "MR 8", "Conference Room"};
    public int selected = 2;
    public CategoryResModel model;
    public int length;
    public String selectedId;
    public List<Resource> resources;
}
