package com.daffodil.bookmeetingroom.activities.homeactivity.model;

import com.daffodil.bookmeetingroom.activities.homeactivity.model.showbookbyres.Schedule;

import java.io.Serializable;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.model . ViewDetailsModel
 * Created by Vishu Bhardwaj on 17/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class ViewDetailsModel extends ShowBookingBaseModel implements Serializable {
    public String time;
    public String label;
    public String description;
    public int from_hh, from_mm;
    public int minutes;
    public boolean isEmpty, isBooked;
    public String meetingRoom;
    public int meetingRoomIndex;
    public int mm, dd, yy;
    public String createdBy;

    public Schedule schedule;
    public boolean isMine, prevBooked;
    public boolean blankTime;

}