package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.categoryresponce.Resource;
import com.daffodil.bookmeetingroom.activities.homeactivity.presenter.HomePresenter;
import com.daffodil.bookmeetingroom.helpers.AppPref;
import com.daffodil.bookmeetingroom.helpers.DateHelper;

import java.util.List;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . BookDeviceFragment
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class BookDeviceFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, iBookDeviceFragment {
    private Context context;
    private Spinner spinner;
    private iHomeActivity activity;
    private View view;
    private EditText etTitle, etDescriptgion;
    private TextView tvMeetingRoom;
    private Button fromDateButton, toDateButton, fromDayButton, toDayButton;
    private CheckBox repeatBooking;
    private RelativeLayout relativeLayout;
    private int dd, mm, yy, hour, minute;
    private List<Resource> resourceList;
    private int position;
    private String[] strings;
    // private

    private int st_h, st_m, et_h, et_m;
    private int st_yy, st_mm, st_dd, et_yy, et_mm, et_dd;


    public void setContext(Context context) {
        this.context = context;
    }

    public void setActivity(iHomeActivity activity) {
        this.activity = activity;
    }

    public void setData(int dd, int mm, int yy, int hour, int minute, List<Resource> resourceList, int position) {
        this.dd = dd;
        this.mm = mm;
        this.yy = yy;
        this.hour = hour;
        this.minute = minute;
        this.st_h = hour;
        this.st_m = minute;
        this.et_h = (hour+(minute+30)/60);
        this.et_m = ((minute+30)%60);


        this.st_yy = yy;
        this.st_mm = mm;
        this.st_dd = dd;

        this.et_yy = yy;
        this.et_mm = mm;
        this.et_dd = dd;

        this.resourceList = resourceList;
        this.position = position;
        this.strings = new String[resourceList.size()];
        for (int i=0; i<resourceList.size(); i++) {
            this.strings[i] = resourceList.get(i).getTitle();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_booking, container, false);
        this.view = view;

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        spinner = (Spinner) view.findViewById(R.id.spinner2);
        if (context == null) {
            context = view.getContext();
        }
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
//                context,
//                R.array.meeting_rooms,
//                android.R.layout.simple_spinner_item);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_spinner_item,
                this.strings
        );
        // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(position);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        repeatBooking = (CheckBox) view.findViewById(R.id.checkBox);
        fromDateButton = (Button) view.findViewById(R.id.btn_from);
        toDateButton = (Button) view.findViewById(R.id.btn_to);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.rl_repeat_booking);
        fromDayButton = (Button) view.findViewById(R.id.btn_from_date);
        toDayButton = (Button) view.findViewById(R.id.btn_to_date);
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        repeatBooking.setOnClickListener(this);
        fromDateButton.setOnClickListener(this);
        toDateButton.setOnClickListener(this);
        fromDayButton.setOnClickListener(this);
        toDayButton.setOnClickListener(this);

        etTitle = (EditText) view.findViewById(R.id.et_title);
        etDescriptgion = (EditText) view.findViewById(R.id.ed_description);
        tvMeetingRoom = (TextView) view.findViewById(R.id.tv_meeting_room);
        tvMeetingRoom.setText(this.resourceList.get(this.position).getTitle());

        String from_date_button = "FROM (" + String.format("%02d", (hour)) + ":" + String.format("%02d", (minute)) + ")";
        fromDateButton.setText(from_date_button);

        String to_date_button = "(" + String.format("%02d", (hour+(minute+30)/60)) + ":" + String.format("%02d", ((minute+30)%60)) + ")";
        toDateButton.setText(to_date_button);

        String from_day_button_text = "(" + String.format("%02d", (dd)) +  "-" + String.format("%02d", (mm)) + "-" + String.format("%4d", (yy)) + ")";
        fromDayButton.setText("FROM "+from_day_button_text);
        toDayButton.setText("TO"+from_day_button_text);

        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnCancel) {
            // getActivity().getFragmentManager().beginTransaction().remove(this).commit();
            activity.onEditBookingCancel();
        } else if(v.getId() == R.id.btnSubmit) {
            onSubmit();
        } else if(v.getId() == R.id.btn_from) {
            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    String date = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-"+String.format("%02d",hourOfDay)+"."+String.format("%02d",minute);
                    long x = DateHelper.dateTimeToTimestamp(date);
                    long y = DateHelper.getCurrentTimestamp();
                    if (y>=x) {
                        Toast.makeText(context, "Can not set time, selected time is less than current time.", Toast.LENGTH_SHORT).show();
                    } else {
                        st_h = hourOfDay;
                        st_m = minute;
                        BookDeviceFragment.this.hour = hourOfDay;
                        BookDeviceFragment.this.minute = minute;
                        et_h = hourOfDay + (minute+30)/60;
                        et_m = (minute+30)%60;
                        fromDateButton.setText("From ("+String.format("%02d",hourOfDay)+"."+String.format("%02d",minute)+")");
                        toDateButton.setText("To ("+String.format("%02d",hourOfDay)+"."+String.format("%02d",minute)+")");
                    }
                }
            };
            CustomTimePickerDialog date = new CustomTimePickerDialog(context, listener, hour, minute, true);
            date.show();
        } else if(v.getId() == R.id.btn_to) {
            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    String date = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-"+String.format("%02d",hourOfDay)+"."+String.format("%02d",minute);
                    long x = DateHelper.dateTimeToTimestamp(date);
                    long y = DateHelper.getCurrentTimestamp();
                    if ((y+30*60*1000)>=x) {
                        Toast.makeText(context, "Can not set time, selected time should be at leaset 30 minute far than current time.", Toast.LENGTH_SHORT).show();
                    } else {
                        et_h = hourOfDay;
                        et_m = minute;
                        toDateButton.setText("To ("+String.format("%02d",hourOfDay)+"."+String.format("%02d",minute)+")");
                    }
                }
            };
            CustomTimePickerDialog date = new CustomTimePickerDialog(context, listener, (hour+(minute+30)/60), ((minute+30)%60), true);
            date.show();
        } else if (v.getId() == R.id.checkBox) {
            if(repeatBooking.isChecked()) {
                relativeLayout.setVisibility(View.VISIBLE);
                Log.d("hasjhdga", "Checked");
            }else {
                relativeLayout.setVisibility(View.GONE);
                Log.d("hasjhdga", "unnnnnnnnnnn Checked");
            }
        } else if(v.getId() == R.id.btn_from_date) {
            DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String d1 = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-00.00";
                    String d2 = String.format("%4d",year)+"-"+String.format("%02d",monthOfYear+1)+"-"+String.format("%02d",dayOfMonth)+"-00.00";
                    if (DateHelper.dateTimeToTimestamp(d1) > DateHelper.dateTimeToTimestamp(d2)) {
                        Toast.makeText(context, "Can not set date, selected date is less than current date.", Toast.LENGTH_SHORT).show();
                    } else {
                        st_yy = year;
                        st_mm = monthOfYear+1;
                        st_dd = dayOfMonth;

                        et_yy = year;
                        et_mm = monthOfYear+1;
                        et_dd = dayOfMonth;

                        fromDayButton.setText("FROM ("+String.format("%02d",dayOfMonth)+"-"+String.format("%02d",monthOfYear+1)+"-"+String.format("%4d",year)+")");
                        toDayButton.setText("TO ("+String.format("%02d",dayOfMonth)+"-"+String.format("%02d",monthOfYear+1)+"-"+String.format("%4d",year)+")");
                    }
                }
            };
            DatePickerFragment datePickerFragment = new DatePickerFragment();
            datePickerFragment.setDate(st_dd, st_mm-1, st_yy);
            datePickerFragment.setListener(listener);
            datePickerFragment.show(getFragmentManager(), "Date from");
        } else if(v.getId() == R.id.btn_to_date) {
            DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String d1 = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-00.00";
                    String d2 = String.format("%4d",year)+"-"+String.format("%02d",monthOfYear+1)+"-"+String.format("%02d",dayOfMonth)+"-00.00";
                    if (DateHelper.dateTimeToTimestamp(d1) > DateHelper.dateTimeToTimestamp(d2)) {
                        Toast.makeText(context, "Can not set date, selected date is less than current date.", Toast.LENGTH_SHORT).show();
                    } else {

                        et_yy = year;
                        et_mm = monthOfYear+1;
                        et_dd = dayOfMonth;

                        toDayButton.setText("TO ("+String.format("%02d",dayOfMonth)+"-"+String.format("%02d",monthOfYear+1)+"-"+String.format("%4d",year)+")");
                    }
                }
            };
            DatePickerFragment datePickerFragment = new DatePickerFragment();
            datePickerFragment.setDate(et_dd, et_mm-1, et_yy);
            datePickerFragment.setListener(listener);
            datePickerFragment.show(getFragmentManager(), "Date to");
        }
    }

    private void onSubmit() {
        if (repeatBooking.isChecked()) {
            // repeat booking
            // simple booking

            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle("Warning !");

            // check for start date
            String date = String.format("%4d",st_yy)+"-"+String.format("%02d",st_mm)+"-"+String.format("%02d",st_dd)+"-"+String.format("%02d",st_h)+"."+String.format("%02d",st_m);
            long x = DateHelper.dateTimeToTimestamp(date);
            String date1 = String.format("%4d",et_yy)+"-"+String.format("%02d",et_mm)+"-"+String.format("%02d",et_dd)+"-"+String.format("%02d",et_h)+"."+String.format("%02d",et_m);
            long x1 = DateHelper.dateTimeToTimestamp(date1);
            long y = DateHelper.getCurrentTimestamp();

            String date2 = String.format("%4d",st_yy)+"-"+String.format("%02d",st_mm)+"-"+String.format("%02d",st_dd)+"-"+String.format("%02d",et_h)+"."+String.format("%02d",et_m);
            long x2 = DateHelper.dateTimeToTimestamp(date2);

            if (y>=x) {
                dialog.setMessage("Can not set time, selected time is less than current time.");
                dialog.show();
            } else if ((y+30*60*1000)>=x1) {
                dialog.setMessage("Can not set time, selected time should be at leaset 30 minute far than current time.");
                dialog.show();
            } else if (etTitle.getText().toString().trim().equals("")) {
                dialog.setMessage("The title can not be empty.");
                dialog.show();
            } else if (etDescriptgion.getText().toString().trim().equals("")) {
                dialog.setMessage("The description can not be empty.");
                dialog.show();
            } else {
                String userId = AppPref.getUserId(context);
                String resourceId = this.resourceList.get(position).getId();
                String fromDate = String.format("%4d",st_yy)+"-"+String.format("%02d",st_mm)+"-"+String.format("%02d",st_dd);
                String toDate = String.format("%4d",et_yy)+"-"+String.format("%02d",et_mm)+"-"+String.format("%02d",et_dd);
                String fromTimestamp = String.valueOf(x);
                String toTimestamp = String.valueOf(x2);
                String title = etTitle.getText().toString().trim();
                String description = etDescriptgion.getText().toString().trim();

                HomePresenter presenter = new HomePresenter(context);
                presenter.tryMultipleBooking(userId, resourceId, fromDate, toDate, fromTimestamp, toTimestamp, title, description, this);
            }

        } else {
            // simple booking

            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle("Warning !");

            // check for start date
            String date = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-"+String.format("%02d",st_h)+"."+String.format("%02d",st_m);
            long x = DateHelper.dateTimeToTimestamp(date);

            String date1 = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd)+"-"+String.format("%02d",et_h)+"."+String.format("%02d",et_m);
            long x1 = DateHelper.dateTimeToTimestamp(date1);

            long y = DateHelper.getCurrentTimestamp();
            if (y>=x) {
                dialog.setMessage("Can not set time, selected time is less than current time.");
                dialog.show();
            } else if ((y+30*60*1000)>=x1) {
                dialog.setMessage("Can not set time, selected time should be at leaset 30 minute far than current time.");
                dialog.show();
            } else if (etTitle.getText().toString().trim().equals("")) {
                dialog.setMessage("The title can not be empty.");
                dialog.show();
            } else if (etDescriptgion.getText().toString().trim().equals("")) {
                dialog.setMessage("The description can not be empty.");
                dialog.show();
            } else {

                        String userId = AppPref.getUserId(context);
                        String resourceId = this.resourceList.get(position).getId();
                        String currDate = String.format("%4d",yy)+"-"+String.format("%02d",mm)+"-"+String.format("%02d",dd);
                        String fromTimestamp = String.valueOf(x);
                        String toTimestamp = String.valueOf(x1);
                        String title = etTitle.getText().toString().trim();
                        String description = etDescriptgion.getText().toString().trim();

                HomePresenter presenter = new HomePresenter(context);
                presenter.trySingleBooking(userId, resourceId, currDate, fromTimestamp, toTimestamp, title, description, this);

            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String newItem = spinner.getItemAtPosition(position).toString();
        tvMeetingRoom.setText(newItem);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void error() {
        activity.onEditBookingCancel();
    }
}
