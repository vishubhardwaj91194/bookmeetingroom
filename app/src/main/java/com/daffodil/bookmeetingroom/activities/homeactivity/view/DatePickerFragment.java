package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . DatePickerFragment
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class DatePickerFragment extends DialogFragment {

    private int dd, mm, yy;
    private DatePickerDialog.OnDateSetListener listener;

    public DatePickerFragment() {

    }

    public void setDate(int dd, int mm, int yy) {
        this.dd = dd;
        this.mm = mm;
        this.yy = yy;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year = yy;
        int month = mm;
        int day = dd;

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }
}