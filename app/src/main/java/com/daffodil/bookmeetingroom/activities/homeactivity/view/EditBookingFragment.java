package com.daffodil.bookmeetingroom.activities.homeactivity.view;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.daffodil.bookmeetingroom.R;
import com.daffodil.bookmeetingroom.activities.homeactivity.model.ViewDetailsModel;

/**
 * com.daffodil.bookmeetingroom.activities.homeactivity.view . EditBookingFragment
 * Created by Vishu Bhardwaj on 18/10/16
 * for http://www.daffodilsw.com
 * Copyright (c) 2016 Daffodil Software Ltd. All rights reserved.
 */
public class EditBookingFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private Spinner spinner;
    private iHomeActivity activity;
    private ViewDetailsModel model;
    private View view;
    private EditText etTitle, etDescriptgion;
    private TextView tvMeetingRoom;
    private Button fromDateButton, toDateButton, fromDayButton, toDayButton;
    private CheckBox repeatBooking;
    private RelativeLayout relativeLayout;


    public void setContext(Context context) {
        this.context = context;
    }

    public void setActivity(iHomeActivity activity) {
        this.activity = activity;
    }

    public void setData(ViewDetailsModel model) {
        this.model = model;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_booking, container, false);
        this.view = view;
        spinner = (Spinner) view.findViewById(R.id.spinner2);
        if (context == null) {
            context = view.getContext();
        }
        if (this.model == null) {
            this.model = (ViewDetailsModel) savedInstanceState.getSerializable("model");
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                context,
                R.array.meeting_rooms,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        repeatBooking = (CheckBox) view.findViewById(R.id.checkBox);
        fromDateButton = (Button) view.findViewById(R.id.btn_from);
        toDateButton = (Button) view.findViewById(R.id.btn_to);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.rl_repeat_booking);
        fromDayButton = (Button) view.findViewById(R.id.btn_from_date);
        toDayButton = (Button) view.findViewById(R.id.btn_to_date);
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        repeatBooking.setOnClickListener(this);
        fromDateButton.setOnClickListener(this);
        toDateButton.setOnClickListener(this);
        fromDayButton.setOnClickListener(this);
        toDayButton.setOnClickListener(this);

        etTitle = (EditText) view.findViewById(R.id.et_title);
        etDescriptgion = (EditText) view.findViewById(R.id.ed_description);
        tvMeetingRoom = (TextView) view.findViewById(R.id.tv_meeting_room);

        etTitle.setText(model.label);
        etDescriptgion.setText(model.description);
        tvMeetingRoom.setText(model.meetingRoom);
        spinner.setSelection(model.meetingRoomIndex);
        String from_date_button = "FROM (" + String.format("%02d", (model.from_hh)) + ":" + String.format("%02d", (model.from_mm)) + ")";
        fromDateButton.setText(from_date_button);
        String to_date_button = "(" + String.format("%02d", (model.from_hh + model.minutes/60)) + ":" + String.format("%02d", (model.from_mm + model.minutes%60)) + ")";
        toDateButton.setText(to_date_button);

        String from_day_button_text = "(" + String.format("%02d", (model.dd)) +  "-" + String.format("%02d", (model.mm)) + "-" + String.format("%4d", (model.yy)) + ")";
        fromDayButton.setText("FROM "+from_day_button_text);
        toDayButton.setText("TO"+from_day_button_text);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnCancel) {
            // getActivity().getFragmentManager().beginTransaction().remove(this).commit();
            activity.onEditBookingCancel();
        } else if(v.getId() == R.id.btnSubmit) {

        } else if(v.getId() == R.id.btn_from) {
            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                }
            };
            CustomTimePickerDialog date = new CustomTimePickerDialog(context, listener, model.from_hh, model.from_mm, true);
            date.show();
        } else if(v.getId() == R.id.btn_to) {
            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                }
            };
            CustomTimePickerDialog date = new CustomTimePickerDialog(context, listener, model.from_hh+model.minutes/60, model.from_mm+model.minutes%60, true);
            date.show();
        } else if (v.getId() == R.id.checkBox) {
            if(repeatBooking.isChecked()) {
                relativeLayout.setVisibility(View.VISIBLE);
                Log.d("hasjhdga", "Checked");
            }else {
                relativeLayout.setVisibility(View.GONE);
                Log.d("hasjhdga", "unnnnnnnnnnn Checked");
            }
        } else if(v.getId() == R.id.btn_from_date) {
            DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                }
            };
            DatePickerFragment datePickerFragment = new DatePickerFragment();
            datePickerFragment.setDate(model.dd, model.mm, model.yy);
            datePickerFragment.setListener(listener);
            datePickerFragment.show(getFragmentManager(), "Date from");
        } else if(v.getId() == R.id.btn_to_date) {
            DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                }
            };
            DatePickerFragment datePickerFragment = new DatePickerFragment();
            datePickerFragment.setDate(model.dd, model.mm, model.yy);
            datePickerFragment.setListener(listener);
            datePickerFragment.show(getFragmentManager(), "Date to");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("model", model);
    }
}
